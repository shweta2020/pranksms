package com.nusys.pranksms.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.WindowManager;

import com.nusys.pranksms.R;
import com.nusys.pranksms.commonModule.Extension;
import com.nusys.pranksms.commonModule.NetworkUtils;

public class Splash extends AppCompatActivity {
    Dialog dialog;
    Context context;

    // SharedPreference_main sharedPreference_main;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        initialization();
        action();
    }

    public void initialization() {

        context = Splash.this;
        // sharedPreference_main = SharedPreference_main.getInstance(context);
        dialog = new Dialog(this);
    }

    public void action() {
        /*for showing splash on full screen
         *Created Date: 22-04-2020
         */
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        /*for intent to main_activity by checking condition whether the net is on or not
         *Created Date: 22-04-2020
         */
        if (NetworkUtils.isConnected(this)) {

            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    startActivity(new Intent(context, Login.class));
                    finish();
                   /* if (sharedPreference_main.getIs_LoggedIn()) {
                        startActivity(new Intent(context, HomeActivity.class));
                        finish();


                    } else {
                        startActivity(new Intent(context, StartCoinbaazar.class));
                        finish();
                    }*/
                }
            }, 4000);
        } else {
            Extension.showErrorDialog(this, dialog);

        }

    }
}
