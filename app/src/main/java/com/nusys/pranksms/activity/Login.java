package com.nusys.pranksms.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.nusys.pranksms.R;

public class Login extends AppCompatActivity {

    Context context;
  //  SharedPreference_main sharedPreference_main;
    TextView tv_forgot_password, tvRegister, tvLogin;
    EditText etEmail, etPassword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        initialization();
        action();
    }


    public void initialization() {

        context = Login.this;
       // sharedPreference_main = SharedPreference_main.getInstance(context);
        tvRegister = findViewById(R.id.tv_register);
        tv_forgot_password = findViewById(R.id.tv_forgotPassword);
        etEmail = findViewById(R.id.et_email);
        etPassword = findViewById(R.id.et_password);
        tvLogin = findViewById(R.id.tv_login);


    }

    public void action() {

        /*redirect to register page when click on tv_register
         * Created Date: 27-04-2020
         */
        tvRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(context, SignUp.class);
                startActivity(i);
            }
        });

        /*redirect to login page when click on bt_login with validation
         * Created Date: 27-04-2020
         */
        tvLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
              /*  if (TextUtils.isEmpty(etEmail.getText().toString())) {

                    etEmail.setError("field can't be empty");

                } else if (TextUtils.isEmpty((etPassword.getText().toString()))) {

                    etPassword.setError("field can't be empty");
                } else {*/
                    Intent i = new Intent(context, MainActivity.class);
                    startActivity(i);
                   // doLogin();

                //}
            }
        });

        /*redirect to forgot password page when click on tv_forgot_password
         * Created Date: 27-04-2020
         */
        tv_forgot_password.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(context, MainActivity.class);
                startActivity(i);
            }
        });


    }
}
