package com.nusys.pranksms.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.os.StrictMode;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.nusys.pranksms.R;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class MainActivity extends AppCompatActivity {

    Button sand;
    EditText message,receiver_phone, sender_phone;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        message=findViewById(R.id.et_message);
        sand=findViewById(R.id.btn_sand);
        receiver_phone=findViewById(R.id.et_receiver_phone);
        sender_phone=findViewById(R.id.et_sender_phone);

        sand.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String phono = receiver_phone.getText().toString();
                String msgtext = message.getText().toString();
                try {
                    // Construct data
                    String apiKey = "apikey=" + "RDUnQLrP+Nc-D7rEtMF7WZyacjPh1aQbEWzVyBx9nC";
                    String message = "&message=" + msgtext;
                    String sender = "&sender=" + "TXTLCL";
                    String numbers = "&numbers=" + phono;
                    // Send data
                    HttpURLConnection conn = (HttpURLConnection) new URL("https://api.textlocal.in/send/?").openConnection();
                    String data = apiKey + numbers + message + sender;
                    conn.setDoOutput(true);
                    conn.setRequestMethod("POST");
                    conn.setRequestProperty("Content-Length", Integer.toString(data.length()));
                    conn.getOutputStream().write(data.getBytes("UTF-8"));
                    final BufferedReader rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                    final StringBuffer stringBuffer = new StringBuffer();
                    String line;
                    while ((line = rd.readLine()) != null) {
                        stringBuffer.append(line);
                        Toast.makeText(MainActivity.this,line,Toast.LENGTH_LONG).show();
                    }
                    rd.close();
                } catch (Exception e) {
                    System.out.println("Error SMS "+e);
                }
            }
        });
        StrictMode.ThreadPolicy st = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(st);
    }
}