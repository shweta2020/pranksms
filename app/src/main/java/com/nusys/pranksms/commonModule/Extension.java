package com.nusys.pranksms.commonModule;

import android.app.Dialog;
import android.content.Context;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.nusys.pranksms.R;


public class Extension {

    public static void showtoast(Context context, String message) {
        Toast.makeText(context, "" + message, Toast.LENGTH_SHORT).show();
    }

    public static void printLog(String msg) {
        Log.e("error", msg);
    }

    public static void printResponse(String msg) {
        Log.e("response", msg);
    }


    public static void showErrorDialog(Context context, final Dialog dialog) {


        dialog.setContentView(R.layout.error_dialog);
        TextView tvOk = dialog.findViewById(R.id.text_ok);
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        dialog.show();

        tvOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });


    }

    public static void showResponseErrorDialog(Context context, final Dialog dialog) {

//        dialog=new Dialog(context);
        dialog.setContentView(R.layout.error_dialog);
        TextView tvOk = dialog.findViewById(R.id.text_ok);
        TextView text = dialog.findViewById(R.id.text);
        text.setText("Something is wrong please try again later");
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        dialog.show();

        tvOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });


    }


}

